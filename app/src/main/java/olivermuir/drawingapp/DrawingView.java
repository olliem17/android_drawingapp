package olivermuir.drawingapp;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;


/**
 * Created by ollie on 20/04/17.
 */


public class DrawingView extends View {

    private Path drawPath;
    private Paint drawPaint, canvasPaint;
    private int paintColor = 0xFF000000;
    private float stroke = 5;
    private int alpha = 255;
    private Canvas drawCanvas;
    private Bitmap canvasBitmap;
    public Boolean draw = true;

    public DrawingView(Context context, AttributeSet attrs){
        super(context, attrs);
        setupDrawing();
    }

    private void setupDrawing(){

        drawPath = new Path();
        drawPaint = new Paint();
        drawPaint.setAntiAlias(true);
        drawPaint.setColor(paintColor);
        drawPaint.setStrokeWidth(stroke);
        drawPaint.setAlpha(alpha);
        drawPaint.setStyle(Paint.Style.STROKE);
        drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);

        canvasPaint = new Paint(Paint.DITHER_FLAG);
    }

    /**
     * Override onSizeChanged method
     */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        canvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        drawCanvas = new Canvas(canvasBitmap);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawBitmap(canvasBitmap, 0, 0, canvasPaint);
        canvas.drawPath(drawPath, drawPaint);
    }



    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (draw) {
            float touchX = event.getX();
            float touchY = event.getY();

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    drawPath.moveTo(touchX, touchY);
                    break;
                case MotionEvent.ACTION_MOVE:
                    drawPath.lineTo(touchX, touchY);
                    break;
                case MotionEvent.ACTION_UP:
                    drawCanvas.drawPath(drawPath, drawPaint);
                    drawPath.reset();
                    break;
                default:
                    return false;
            }
            invalidate();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Set the paint color when clicked
     */
    public void setColor(String newColor){
        invalidate();
        paintColor = Color.parseColor(newColor);
        drawPaint.setColor(paintColor);
        drawPaint.setAlpha(alpha);
    }


    /**
     * Set the stroke width
     */
    public void setStroke(int newStroke){
        invalidate();
        stroke = newStroke;
        drawPaint.setStrokeWidth(stroke);
    }

    /**
     * Set the stroke width
     */
    public void setAlpha(int newAlpha){
        invalidate();
        alpha = newAlpha;
        drawPaint.setAlpha(alpha);
    }

    public void setStyle(String style) {

        switch (style) {
            case "spray":
                //NORMAL, SOLID, OUTER, INNER
                drawPaint.setMaskFilter(new BlurMaskFilter(16, BlurMaskFilter.Blur.NORMAL));
                break;
            case "pencil":
                drawPaint.setMaskFilter(null);
                break;

            default:

        }
    }

    public void newDrawing(){
        drawCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
        invalidate();
    }

    public void setImage(Bitmap bitmap) {
        drawCanvas.drawBitmap(bitmap, 0, 0, null);
    }



}
