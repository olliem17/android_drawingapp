package olivermuir.drawingapp;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.UUID;

import static java.lang.Boolean.FALSE;
import static olivermuir.drawingapp.R.id.alphaBar;
import static olivermuir.drawingapp.R.id.strokeBar;

public class MainActivity extends AppCompatActivity implements OnClickListener, SeekBar.OnSeekBarChangeListener {

    private DrawingView drawView;
    private ImageButton currPaint, newBtn, drawBtn, colorBtn, imageBtn, saveBtn, paintSelected;
    private SeekBar strokeSlider, alphaSlider;
    private int seekBarProgress = 0;
    private TextView txtStroke, txtAlpha;
    private ImageView img;
    private LinearLayout nav, colorLayout, brushLayout;
    private float smallBrush, mediumBrush, largeBrush;

    private Boolean color_layout = FALSE;

    private static int RESULT_LOAD_IMG = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        drawView = (DrawingView)findViewById(R.id.drawing);
        paintSelected = (ImageButton)findViewById(R.id.paint_selected);
        newBtn = (ImageButton)findViewById(R.id.new_btn);
        newBtn.setOnClickListener(this);
        drawBtn = (ImageButton)findViewById(R.id.brush_btn);
        drawBtn.setOnClickListener(this);
        colorBtn = (ImageButton)findViewById(R.id.color_btn);
        colorBtn.setOnClickListener(this);
        saveBtn = (ImageButton)findViewById(R.id.save_btn);
        saveBtn.setOnClickListener(this);
        imageBtn = (ImageButton) findViewById(R.id.image_btn);
        imageBtn.setOnClickListener(this);

        img = (ImageView) findViewById(R.id.img);

        smallBrush = getResources().getInteger(R.integer.small_size);
        mediumBrush = getResources().getInteger(R.integer.medium_size);
        largeBrush = getResources().getInteger(R.integer.large_size);

        nav = (LinearLayout) findViewById(R.id.nav);
        colorLayout = (LinearLayout)findViewById(R.id.color_layout);
        colorLayout.setVisibility(View.INVISIBLE);
        brushLayout = (LinearLayout)findViewById(R.id.brush_layout);
        brushLayout.setVisibility(View.INVISIBLE);

        strokeSlider = (SeekBar) findViewById(strokeBar);
        strokeSlider.setOnSeekBarChangeListener(this);
        txtStroke = (TextView) findViewById(R.id.countStroke);

        alphaSlider = (SeekBar) findViewById(alphaBar);
        alphaSlider.setOnSeekBarChangeListener(this);
        txtAlpha = (TextView) findViewById(R.id.countAlpha);

    }


    public void paintClicked(View view){

        Log.d("PAINT", "Clicked");

        if(view != currPaint){

            ImageButton imgView = (ImageButton)view;
            String color = view.getTag().toString();
            drawView.setColor(color);
            paintSelected.setBackgroundColor(Color.parseColor(color));


            ImageView viewStroke = (ImageView) findViewById(R.id.stroke);
            viewStroke.setColorFilter(Color.parseColor(color));
            //viewStroke.setStroke(3);
            //Drawable drawable = getResources().getDrawable(R.drawable.stroke2);
            //drawable.setStrokeWidth(20);

        }
    }

    public void brushClicked(View view){

        Log.d("BRUSH", "Clicked");
        String style = view.getTag().toString();
        drawView.setStyle(style);
    }

    public void close(View view){
        Log.d("CLOSE", "Clicked");
        enableButtons(true);
        brushLayout.setVisibility(View.INVISIBLE);
        colorLayout.setVisibility(View.INVISIBLE);

    }


    @Override
    public void onClick(View view){

        Log.d("Click", String.valueOf(view.getId()));

        // New drawing button
        if(view.getId()==R.id.new_btn){
            AlertDialog.Builder newDialog = new AlertDialog.Builder(this);
            newDialog.setTitle("New drawing");
            newDialog.setMessage("Start new drawing (you will lose the current drawing)?");
            newDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialog, int which){
                    drawView.newDrawing();
                    dialog.dismiss();
                }
            });
            newDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialog, int which){
                    dialog.cancel();
                }
            });
            newDialog.show();
        }

        // Brush button
        else if(view.getId()==R.id.brush_btn){

            Log.d("Click", "Brush button clicked");

            enableButtons(false);


            if (brushLayout.getVisibility() == View.VISIBLE) {

                Log.d("ColorLayout", "VISLBLE");
                brushLayout.setVisibility(View.INVISIBLE);

            } else {

                Log.d("ColorLayout", "INVISLBLE");
                brushLayout.setVisibility(View.VISIBLE);
            }


        }

        // Color Button
        else if (view.getId()==R.id.color_btn) {

            Log.d("Click", "Color button clicked");

            enableButtons(false);

            if (colorLayout.getVisibility() == View.VISIBLE) {

                Log.d("ColorLayout", "VISLBLE");
                colorLayout.setVisibility(View.INVISIBLE);

            } else {

                Log.d("ColorLayout", "INVISLBLE");
                colorLayout.setVisibility(View.VISIBLE);
            }

        // Picture Button
        } else if (view.getId()==R.id.image_btn) {

            Log.d("Click", "Picture button clicked");

            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, RESULT_LOAD_IMG);


        }
        // Save Button
        else if (view.getId()==R.id.save_btn) {

            Log.d("Click", "Save button clicked");

            AlertDialog.Builder saveDialog = new AlertDialog.Builder(this);
            saveDialog.setTitle("Save drawing");
            saveDialog.setMessage("Save drawing to device Gallery?");
            saveDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialog, int which){
                    //save drawing
                    drawView.setDrawingCacheEnabled(true);
                    String imgSaved = MediaStore.Images.Media.insertImage(getContentResolver(), drawView.getDrawingCache(), UUID.randomUUID().toString()+".png", "drawing");
                    if(imgSaved!=null){
                        Toast savedToast = Toast.makeText(getApplicationContext(),
                                "Drawing saved to Gallery.", Toast.LENGTH_SHORT);
                        savedToast.show();
                    }
                    else {
                        Toast unsavedToast = Toast.makeText(getApplicationContext(),
                                "Oops! Image could not be saved.", Toast.LENGTH_SHORT);
                        unsavedToast.show();
                    }
                    drawView.destroyDrawingCache();
                }
            });
            saveDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialog, int which){
                    dialog.cancel();
                }
            });
            saveDialog.show();


        }
    }

    // SeekBar Listener
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        seekBarProgress = progress;

        switch (seekBar.getId()) {

            case R.id.strokeBar:
                txtStroke.setText(String.valueOf(seekBarProgress));
                break;

            case R.id.alphaBar:
                txtAlpha.setText(String.valueOf(seekBarProgress));
                break;
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

        switch (seekBar.getId()) {

            case R.id.strokeBar:
                Log.d("SeekBar", "Stroke Changed: " +seekBarProgress );
                txtStroke.setText(String.valueOf(seekBarProgress));
                drawView.setStroke(seekBarProgress);
                break;

           case R.id.alphaBar:
               Log.d("SeekBar", "Alpha Changed: "+seekBarProgress);
               txtAlpha.setText(String.valueOf(seekBarProgress));
               drawView.setAlpha(seekBarProgress);
               break;
        }
    }





    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMG) {
            if (resultCode == Activity.RESULT_OK) {
                // do something. play video using uri
                Log.d("GetImage", "Got Image");
                Uri selectedImage = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                    //img.setImageBitmap(bitmap);
                    drawView.setImage(scaleToActualAspectRatio(bitmap));
                } catch (IOException e) {
                    Log.i("TAG", "Some exception " + e);
                }
            }

        }
    }

    public Bitmap scaleToActualAspectRatio(Bitmap bitmap) {
        if (bitmap != null) {
            boolean flag = true;

            int deviceWidth = getWindowManager().getDefaultDisplay()
                    .getWidth();
            int deviceHeight = getWindowManager().getDefaultDisplay()
                    .getHeight();

            int bitmapHeight = bitmap.getHeight();
            int bitmapWidth = bitmap.getWidth();

            if (bitmapWidth > deviceWidth) {
                flag = false;

                // scale According to WIDTH
                int scaledWidth = deviceWidth;
                int scaledHeight = (scaledWidth * bitmapHeight) / bitmapWidth;

                try {
                    if (scaledHeight > deviceHeight)
                        scaledHeight = deviceHeight;
                    bitmap = Bitmap.createScaledBitmap(bitmap, scaledWidth,
                            scaledHeight, true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (flag) {
                if (bitmapHeight > deviceHeight) {
                    // scale According to HEIGHT
                    int scaledHeight = deviceHeight;
                    int scaledWidth = (scaledHeight * bitmapWidth) / bitmapHeight;

                    try {
                        if (scaledWidth > deviceWidth)
                            scaledWidth = deviceWidth;
                        bitmap = Bitmap.createScaledBitmap(bitmap, scaledWidth,
                                scaledHeight, true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return bitmap;
    }

    public void enableButtons(Boolean boo) {

        newBtn.setClickable(boo);
        drawBtn.setClickable(boo);
        colorBtn.setClickable(boo);
        saveBtn.setClickable(boo);
        imageBtn.setClickable(boo);

        drawView.draw = boo;
    }



}
